# PG and QL algorithms in Atari environemnt

Please run the .ipynb files  which include installations.

PGQL algorithm is based on the following paper: 
 - COMBINING POLICY GRADIENT AND Q-LEARNING by Brendan O’Donoghue, Remi Munos, Koray Kavukcuoglu & Volodymyr Mnih

[![Implementation Video](http://img.youtube.com/vi/gZh-ReS0gGM/0.jpg)](http://www.youtube.com/watch?v=gZh-ReS0gGM )

Project is a part of Reinforcement Learning course in IITK