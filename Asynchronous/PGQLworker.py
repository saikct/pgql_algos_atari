import torch.multiprocessing as mp
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import gym
import numpy as np
import torch

"""
Functions that use multiple times
"""

def v_wrap(np_array, dtype=np.float32):
    if np_array.dtype != dtype:
        np_array = np_array.astype(dtype)
    return torch.from_numpy(np_array)


def set_init(layers):
    for layer in layers:
        nn.init.normal_(layer.weight)
        nn.init.constant_(layer.bias, 0.)


def push_and_pull_pgql(opt,lopt, lnet, gnet, done, s_, bs, ba, br,bq, gamma,global_update=False):
    if done:
        v_s_ = 0.0              # terminal
    else:
        _,v_s_,_ = lnet.forward(v_wrap(s_[None, :]))
        v_s_=v_s_.detach().numpy()
        #print(v_s_,type(v_s_))
        #s_tensor=torch.from_numpy(s_).float().unsqueeze(0)
        #v_s_

    buffer_v_target = []
    for r in br[::-1]:    # reverse buffer r
        v_s_ = r + gamma * v_s_
        buffer_v_target.append(v_s_)
    buffer_v_target.reverse()

    a_loss,c_loss,q_loss = lnet.loss_func(
        v_wrap(np.vstack(bs)),
        v_wrap(np.array(ba), dtype=np.int64) if ba[0].dtype == np.int64 else v_wrap(np.vstack(ba)),
        v_wrap(np.vstack(buffer_v_target)),
        v_wrap(np.vstack(bq)))

    # calculate local gradients and push local parameters to global
    if global_update:
        opt.zero_grad()
        #loss=np.mean([a_loss,c_loss])
        a_loss.backward(retain_graph=True)
        c_loss.backward(retain_graph=True)
        q_loss.backward()
        for lp, gp in zip(lnet.parameters(), gnet.parameters()):
            gp._grad = lp.grad
        opt.step()
        # pull global parameters
        lnet.load_state_dict(gnet.state_dict())
    else:
        lopt.zero_grad()
        a_loss.backward(retain_graph=True)
        c_loss.backward(retain_graph=True)
        #lopt.step()
        #lopt.zero_grad()
        q_loss.backward()
        lopt.step()



def record(global_ep, global_ep_r, ep_r, res_queue, name):
    with global_ep.get_lock():
        global_ep.value += 1
    with global_ep_r.get_lock():
        if global_ep_r.value == 0.:
            global_ep_r.value = ep_r
        else:
            global_ep_r.value = global_ep_r.value*(global_ep.value-1)/global_ep.value+ep_r/global_ep.value  
            #global_ep_r.value = ep_r
    res_queue.put(global_ep_r.value)
    if global_ep.value %10 == 0:
        print(
            name,
            "Ep:", global_ep.value,
            "| Ep_r: %.0f" % global_ep_r.value,
        )

"""
Shared optimizer, the parameters in the optimizer will shared in the multiprocessors.
"""

class PGQLSharedAdam(torch.optim.Adam):
    def __init__(self, actor_params, critic_params, q_params, lr_actor=2e-3, lr_critic=2e-3, lr_q=1e-3):
        # Create parameter groups
        params = [{'params': actor_params, 'lr': lr_actor},
                  {'params': critic_params, 'lr': lr_critic},
                  {'params': q_params, 'lr': lr_q}]
        super(PGQLSharedAdam, self).__init__(params)
        # State initialization
        for group in self.param_groups:
            for p in group['params']:
                state = self.state[p]
                state['step'] = torch.tensor(0)
                state['exp_avg'] = torch.zeros_like(p.data)
                state['exp_avg_sq'] = torch.zeros_like(p.data)

                # share in memory
                state['exp_avg'].share_memory_()
                state['exp_avg_sq'].share_memory_()


class PGQLNet(nn.Module):
    def __init__(self, num_inputs, num_actions,hidden_size=128):
        super(PGQLNet, self).__init__()
        self.s_dim = num_inputs
        self.a_dim = num_actions
        self.fc1 = nn.Linear(num_inputs, hidden_size)
        # Actor's layer
        self.a1 = nn.Linear(hidden_size, hidden_size)
        self.actor = nn.Linear(hidden_size, num_actions)
        # Critic's layer
        self.v1 = nn.Linear(hidden_size, hidden_size)
        self.critic = nn.Linear(hidden_size, 1)
	    # Q-learning layer
        self.q1 = nn.Linear(hidden_size*2, hidden_size)
        self.q = nn.Linear(hidden_size, num_actions)
        set_init([self.fc1, self.a1, self.actor, self.v1,self.critic,self.q1,self.q])
        self.distribution = torch.distributions.Categorical

    def forward(self, x):
        # Common forward pass
        x = x.view(x.size(0), -1)# flatten input since multi-dimenstional
        x = F.tanh(self.fc1(x))

        # Actor's forward pass
        #action_probs = F.softmax(self.actor(x), dim=-1)
        a=F.relu(self.a1(x))
        logits = self.actor(a)
        
        # Critic's forward pass
        v=F.tanh(self.v1(x))
        state_value = self.critic(v)

         # Q-learning forward pass
        q_input = torch.cat((a, v),dim=-1)
        #print(q_input.shape)
        q = F.relu(self.q1(q_input))
        q_values = self.q(q)

        return logits, state_value , q_values

    def choose_action(self, s):
        self.eval()
        logits, _,_ = self.forward(s)
        # Normalize logits
        logits = (logits - logits.mean()) / (logits.std() + 1e-5)
        temp=0.9
        prob = F.softmax(logits/temp, dim=1).data
        #action_probs = F.softmax(logits / temp, dim=-1)
        #print(prob)
        m = self.distribution(prob)
        #print(m)
        #return action_probs
        return m.sample().numpy()[0]

    def loss_func(self, s, a, v_t,q_t):
        self.train()
        logits, values,q_values = self.forward(s)
        td = v_t.detach() - values
        c_loss = td.pow(2)

        probs = F.softmax(logits, dim=1)
        m = self.distribution(probs)
        exp_v = m.log_prob(a) * td
        a_loss = -exp_v

        delta = q_values[range(len(q_values)),a] -q_t.detach()
        q_loss = delta.pow(2)

        #total_loss = (c_loss + a_loss).mean()
        return a_loss.mean(), c_loss.mean(),q_loss.mean()
        #return total_loss


class PGQLWorker(mp.Process):
    def __init__(self, gnet, opt, global_ep, global_ep_r, res_queue, name,env_name,N_S,N_A,global_iter,local_iter,max_ep,gamma,learning_rate):
        super(PGQLWorker, self).__init__()
        self.name = 'w%02i' % name
        self.g_ep, self.g_ep_r, self.res_queue = global_ep, global_ep_r, res_queue
        self.gnet, self.opt= gnet,opt
        self.lnet = PGQLNet(N_S, N_A)           # local network
        self.env = gym.make(env_name, render_mode='rgb_array').unwrapped
        self.lopt=PGQLSharedAdam(self.lnet.actor.parameters(), self.lnet.critic.parameters(),self.lnet.q.parameters(), lr_actor=learning_rate, lr_critic=learning_rate, lr_q=learning_rate/2)      # global optimizer
        self.global_upd_iter=global_iter
        self.max_ep=max_ep
        self.local_update_iter=local_iter
        self.gamma=gamma
    def run(self):
        total_step = 1
        global_update_iter=self.global_upd_iter
        while self.g_ep.value < self.max_ep:
            s = self.env.reset()[0]
            buffer_s, buffer_a, buffer_r, buffer_q = [], [], [], []
            ep_r = 0.
            while True:
                if self.name == 'w00':
                    self.env.render()
                a = self.lnet.choose_action(v_wrap(s[None,:]))
                #a=self.lnet.choose_action(s)
                #print(a)
                #a=action+1
                s_, r, done, _,_ = self.env.step(a)
                ep_r += r
                if done:
                    q=r+0.0
                else:
                    _,_,next_qs = self.lnet.forward(v_wrap(s_[None,:]))
                    q=r+self.gamma * np.max(next_qs.detach().numpy())
                #print(r)
                buffer_a.append(a)
                buffer_s.append(s)
                buffer_r.append(r)
                buffer_q.append(q)

                if total_step % self.local_update_iter ==0 or done:
                    if total_step % global_update_iter == 0 or done:  # update global and assign to local net
                        # sync
                        push_and_pull_pgql(self.opt,self.lopt, self.lnet, self.gnet, done, s_, buffer_s, buffer_a, buffer_r,buffer_q, self.gamma,True)
                        buffer_s, buffer_a, buffer_r,buffer_q = [], [], [], []
                        if done:  # done and print information
                            record(self.g_ep, self.g_ep_r, ep_r, self.res_queue, self.name)
                            break
                    else:
                        push_and_pull_pgql(self.opt, self.lopt,self.lnet, self.gnet, done, s_, buffer_s, buffer_a, buffer_r,buffer_q, self.gamma)
                        buffer_s, buffer_a, buffer_r,buffer_q = [], [], [], []
                s = s_
                total_step += 1
        self.res_queue.put(None)
