import torch.multiprocessing as mp
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import gym
import numpy as np
import torch

"""
Functions that use multiple times
"""

def v_wrap(np_array, dtype=np.float32):
    if np_array.dtype != dtype:
        np_array = np_array.astype(dtype)
    return torch.from_numpy(np_array)


def set_init(layers):
    for layer in layers:
        nn.init.normal_(layer.weight)
        nn.init.constant_(layer.bias, 0.)


def push_and_pull_dqn(opt, lopt,lnet, gnet, bs, ba, bq,global_update=False):
    #print(type(bq))
    loss = lnet.loss_func(
        v_wrap(np.vstack(bs)),
        (ba),
        v_wrap(np.vstack(bq)))

    # calculate local gradients and push local parameters to global
    '''
    opt.zero_grad()
    #loss=np.mean([a_loss,c_loss])
    loss.backward()
    #loss.backward()
    for lp, gp in zip(lnet.parameters(), gnet.parameters()):
        gp._grad = lp.grad
    opt.step()

    # pull global parameters
    lnet.load_state_dict(gnet.state_dict())
    '''
    # calculate local gradients and push local parameters to global
    if global_update:
        opt.zero_grad()
        loss.backward()
        for lp, gp in zip(lnet.parameters(), gnet.parameters()):
            gp._grad = lp.grad
        opt.step()
        # pull global parameters
        lnet.load_state_dict(gnet.state_dict())
    else:
        lopt.zero_grad()
        loss.backward()
        lopt.step()


def record(global_ep, global_ep_r, ep_r, res_queue, name):
    with global_ep.get_lock():
        global_ep.value += 1
    with global_ep_r.get_lock():
        if global_ep_r.value == 0.:
            global_ep_r.value = ep_r
        else:
            global_ep_r.value = global_ep_r.value*(global_ep.value-1)/global_ep.value+ep_r/global_ep.value  
            #global_ep_r.value = ep_r
    res_queue.put(global_ep_r.value)
    if global_ep.value %10 == 0:
        print(
            name,
            "Ep:", global_ep.value,
            "| Ep_r: %.0f" % global_ep_r.value,
        )

"""
Shared optimizer, the parameters in the optimizer will shared in the multiprocessors.
"""

class DQNSharedAdam(torch.optim.Adam):
    def __init__(self, params, lr=2e-3):
        super(DQNSharedAdam, self).__init__(params, lr=lr)
        # State initialization
        for group in self.param_groups:
            for p in group['params']:
                state = self.state[p]
                state['step'] = torch.tensor(0)
                state['exp_avg'] = torch.zeros_like(p.data)
                state['exp_avg_sq'] = torch.zeros_like(p.data)

                # share in memory
                state['exp_avg'].share_memory_()
                state['exp_avg_sq'].share_memory_()


class DQNet(nn.Module):
    def __init__(self, num_inputs, num_actions,hidden_size=128):
        super(DQNet, self).__init__()
        self.s_dim = num_inputs
        self.a_dim = num_actions
        self.fc1=nn.Linear(num_inputs, hidden_size)
        self.fc3=nn.Linear(hidden_size, num_actions)
        set_init([self.fc1,self.fc3])
        self.distribution = torch.distributions.Categorical

    def forward(self, x):
        x = x.view(x.size(0), -1)# flatten input since multi-dimenstional
        x=self.fc1(x)
        x=F.relu(x)
        q_values=self.fc3(x)
        return q_values

    def choose_action(self,env, s, epsilon):
        self.eval()
        if np.random.rand() < epsilon:
            action = env.action_space.sample()
        else:
            q_values= self.forward(s)
            #print(q_values)
            #print(np.max(q_values.detach().numpy()))
            #_, action = torch.max(q_values, dim=0)
            action =np.argmax(v_wrap(q_values.detach().numpy()))
            action = int(action.item())
        return action

    def loss_func(self, s, a, q_t):
        self.train()
        #print(type(a))
        q_values = self.forward(s)
        a=torch.tensor(a, dtype=torch.int64) 
        delta = q_values[range(len(q_values)),a] -q_t.detach()
        loss = delta.pow(2).mean()
        return loss


class DQNWorker(mp.Process):
    def __init__(self, gnet, opt, global_ep, global_ep_r, res_queue, name,env_name,N_S,N_A,global_iter,local_iter,max_ep,gamma,epsilon,learning_rate):
        super(DQNWorker, self).__init__()
        self.name = 'w%02i' % name
        self.g_ep, self.g_ep_r, self.res_queue = global_ep, global_ep_r, res_queue
        self.gnet, self.opt = gnet, opt
        self.lnet = DQNet(N_S, N_A)           # local network
        self.env = gym.make(env_name, render_mode='rgb_array').unwrapped
        self.lopt= DQNSharedAdam(self.lnet.parameters(), lr=learning_rate)
        self.global_upd_iter=global_iter
        self.max_ep=max_ep
        self.local_update_iter=local_iter
        self.gamma=gamma
        self.epsilon=epsilon
    def run(self):
        total_step = 1
        global_update_iter=self.global_upd_iter
        while self.g_ep.value < self.max_ep:
            s = self.env.reset()[0]
            buffer_s, buffer_a, buffer_q = [], [], []
            ep_r = 0.
            while True:
                if self.name == 'w00':
                    self.env.render()
                a = self.lnet.choose_action(self.env,v_wrap(s[None,:]),self.epsilon)
                #print(a,type(a))
                #a=self.lnet.choose_action(s)
                #print(a)
                #a=action+1
                s_, r, done, _,_ = self.env.step(a)
                ep_r += r
                if done:
                    q=r+0.0
                else:
                    next_qs = self.lnet.forward(v_wrap(s_[None,:])).detach().numpy()
                    q=r+self.gamma * np.max(next_qs)
                #print(r)
                buffer_a.append(a)
                buffer_s.append(s)
                buffer_q.append(q)


                if total_step % self.local_update_iter ==0 or done:
                    if total_step % global_update_iter == 0 or done:  # update global and assign to local net
                        # sync
                        push_and_pull_dqn(self.opt,self.lopt, self.lnet, self.gnet,  buffer_s, buffer_a, buffer_q, True)
                        buffer_s, buffer_a, buffer_q= [], [], []
                        if done:  # done and print information
                            record(self.g_ep, self.g_ep_r, ep_r, self.res_queue, self.name)
                            break
                    else:
                        push_and_pull_dqn(self.opt, self.lopt,self.lnet, self.gnet,  buffer_s, buffer_a, buffer_q)
                        buffer_s, buffer_a, buffer_q= [], [], []
                s = s_
                total_step += 1
        self.res_queue.put(None)
