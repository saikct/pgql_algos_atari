import torch.multiprocessing as mp
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import gym
import numpy as np
import torch

"""
Functions that use multiple times
"""

def v_wrap(np_array, dtype=np.float32):
    if np_array.dtype != dtype:
        np_array = np_array.astype(dtype)
    return torch.from_numpy(np_array)


def set_init(layers):
    for layer in layers:
        nn.init.normal_(layer.weight)
        nn.init.constant_(layer.bias, 0.)


def push_and_pull_a3c(opt, lopt, lnet, gnet, done, s_, bs, ba, br, gamma,global_update=False):
    if done:
        v_s_ = 0.               # terminal
    else:
        v_s_ = lnet.forward(v_wrap(s_[None, :]))[-1].data.numpy()[0, 0]
        #s_tensor=torch.from_numpy(s_).float().unsqueeze(0)
        #v_s_

    buffer_v_target = []
    for r in br[::-1]:    # reverse buffer r
        v_s_ = r + gamma * v_s_
        buffer_v_target.append(v_s_)
    buffer_v_target.reverse()

    a_loss,c_loss = lnet.loss_func(
        v_wrap(np.vstack(bs)),
        v_wrap(np.array(ba), dtype=np.int64) if ba[0].dtype == np.int64 else v_wrap(np.vstack(ba)),
        v_wrap(np.array(buffer_v_target)[:, None]))

    # calculate local gradients and push local parameters to global
    '''
    opt.zero_grad()
    #loss=np.mean([a_loss,c_loss])
    a_loss.backward(retain_graph=True)  # Retain computation graph for critic update
    c_loss.backward()
    #loss.backward()
 
    for lp, gp in zip(lnet.parameters(), gnet.parameters()):
        gp._grad = lp.grad
    opt.step()

    # pull global parameters
    lnet.load_state_dict(gnet.state_dict())
    '''
    # calculate local gradients and push local parameters to global
    if global_update:
        opt.zero_grad()
        #loss=np.mean([a_loss,c_loss])
        a_loss.backward(retain_graph=True)
        c_loss.backward()
        for lp, gp in zip(lnet.parameters(), gnet.parameters()):
            gp._grad = lp.grad
        opt.step()
        # pull global parameters
        lnet.load_state_dict(gnet.state_dict())
    else:
        lopt.zero_grad()
        a_loss.backward(retain_graph=True)
        c_loss.backward()
        lopt.step()



def record(global_ep, global_ep_r, ep_r, res_queue, name):
    with global_ep.get_lock():
        global_ep.value += 1
    with global_ep_r.get_lock():
        if global_ep_r.value == 0.:
            global_ep_r.value = ep_r
        else:
            global_ep_r.value = global_ep_r.value*(global_ep.value-1)/global_ep.value+ep_r/global_ep.value  
            #global_ep_r.value = ep_r
    res_queue.put(global_ep_r.value)
    if global_ep.value %10 == 0:
        print(
            name,
            "Ep:", global_ep.value,
            "| Ep_r: %.0f" % global_ep_r.value,
        )

"""
Shared optimizer, the parameters in the optimizer will shared in the multiprocessors.
"""

class A3CSharedAdam(torch.optim.Adam):
    def __init__(self, params, lr=2e-3):
        super(A3CSharedAdam, self).__init__(params, lr=lr)
        # State initialization
        for group in self.param_groups:
            for p in group['params']:
                state = self.state[p]
                state['step'] = torch.tensor(0)
                state['exp_avg'] = torch.zeros_like(p.data)
                state['exp_avg_sq'] = torch.zeros_like(p.data)

                # share in memory
                state['exp_avg'].share_memory_()
                state['exp_avg_sq'].share_memory_()


class A3CNet(nn.Module):
    def __init__(self, num_inputs, num_actions,hidden_size=128):
        super(A3CNet, self).__init__()
        self.s_dim = num_inputs
        self.a_dim = num_actions
        self.fc1 = nn.Linear(num_inputs, hidden_size)
        # Actor's layer
        self.a1 = nn.Linear(hidden_size, hidden_size)
        self.actor = nn.Linear(hidden_size, num_actions)
        # Critic's layer
        self.v1 = nn.Linear(hidden_size, hidden_size)
        self.critic = nn.Linear(hidden_size, 1)
        set_init([self.fc1, self.a1, self.actor, self.v1,self.critic])
        self.distribution = torch.distributions.Categorical

    def forward(self, x):
        # Common forward pass
        x = x.view(x.size(0), -1)# flatten input since multi-dimenstional
        x = F.tanh(self.fc1(x))
        # Actor's forward pass
        #action_probs = F.softmax(self.actor(x), dim=-1)
        a=F.relu(self.a1(x))
        logits = self.actor(a)
        # Critic's forward pass
        v=F.tanh(self.v1(x))
        state_values = self.critic(v)
        return logits, state_values

    def choose_action(self, s):
        self.eval()
        #s_tensor=torch.from_numpy(s).float().unsqueeze(0)
        #logits, _ = self.forward(s_tensor)

        logits, _ = self.forward(s)
        # Normalize logits
        logits = (logits - logits.mean()) / (logits.std() + 1e-5)
        temp=0.9
        prob = F.softmax(logits/temp, dim=1).data
        #action_probs = F.softmax(logits / temp, dim=-1)
        #print(prob)
        m = self.distribution(prob)
        #print(m)
        #return action_probs
        return m.sample().numpy()[0]

    def loss_func(self, s, a, v_t):
        self.train()
        logits, values = self.forward(s)
        delta = v_t - values
        c_loss = delta.pow(2)

        probs = F.softmax(logits, dim=1)
        m = self.distribution(probs)
        exp_v = m.log_prob(a) * delta
        a_loss = -exp_v
        #total_loss = (c_loss + a_loss).mean()
        return a_loss.mean(), c_loss.mean()
        #return total_loss


class A3CWorker(mp.Process):
    def __init__(self, gnet, opt, global_ep, global_ep_r, res_queue, name,env_name,N_S,N_A,global_iter,local_iter,max_ep,gamma,learning_rate):
        super(A3CWorker, self).__init__()
        self.name = 'w%02i' % name
        self.g_ep, self.g_ep_r, self.res_queue = global_ep, global_ep_r, res_queue
        self.gnet, self.opt = gnet, opt
        self.lnet = A3CNet(N_S, N_A)           # local network
        self.env = gym.make(env_name, render_mode='rgb_array').unwrapped
        self.lopt= A3CSharedAdam(self.lnet.parameters(), lr=learning_rate)
        self.global_upd_iter=global_iter
        self.local_update_iter=local_iter
        self.max_ep=max_ep
        self.gamma=gamma
    def run(self):
        total_step = 1
        global_update_iter=self.global_upd_iter
        while self.g_ep.value < self.max_ep:
            s = self.env.reset()[0]
            buffer_s, buffer_a, buffer_r = [], [], []
            ep_r = 0.
            while True:
                if self.name == 'w00':
                    self.env.render()
                a = self.lnet.choose_action(v_wrap(s[None,:]))
                #a=self.lnet.choose_action(s)
                #print(a)
                #a=action+1
                s_, r, done, _,_ = self.env.step(a)
                ep_r += r
                #print(r)
                buffer_a.append(a)
                buffer_s.append(s)
                buffer_r.append(r)

                
                if total_step % self.local_update_iter ==0 or done:
                    if total_step % global_update_iter == 0 or done:  # update global and assign to local net
                        # sync
                        push_and_pull_a3c(self.opt,self.lopt, self.lnet, self.gnet, done, s_, buffer_s, buffer_a, buffer_r, self.gamma,True)
                        buffer_s, buffer_a, buffer_r= [], [], []
                        if done:  # done and print information
                            record(self.g_ep, self.g_ep_r, ep_r, self.res_queue, self.name)
                            break
                    else:
                        push_and_pull_a3c(self.opt, self.lopt,self.lnet, self.gnet, done, s_, buffer_s, buffer_a, buffer_r, self.gamma)
                        buffer_s, buffer_a, buffer_r= [], [], []
                s = s_
                total_step += 1
        self.res_queue.put(None)
